from components import Environment
from lib import read_ui

def provide_environment(interface_type=None):
    """Provides a combination of static settings and the user requested parameters"""
    user_params = read_ui(interface_type)

    return Environment

