(defvar signature-length 6)
(defun sign-task (from to)
  (interactive "r")
  (let* ((from from)
	 (to to)
	 (signature (substring (sha1 (buffer-substring from to))
			       0 signature-length))
	 (reference (format "[[#%s][" signature)))
    (goto-char from)
    (insert reference)
    (goto-char (+ to (length reference)))
    (insert "]")))

* Sign a section

#. [[#5f5eed][section - select]] @selected
#. [[#ed243d][section: paragraph@each - sign]]
#. [[#91d54c][section: @selected - sign]]
#. [[#d077a5][section: signature - insert]]

** Sign each paragraph
   :PROPERTIES:
   :CUSTOM_ID: ed243d
   :END:

#. Select a paragraph
#. Normalize text of a paragraph
#. Sign with sha1
#. Insert the signature above the paragraph

*** Select a section
    :PROPERTIES:
    :CUSTOM_ID: 5f5eed
    :END:


#. Start selecting after the new line character
#. Keep selecting until the next sequence of new line characters or EOF

*** Paragraph: text - normalize

#. Remove all punctuation
#. Remove all whitespace and line breaks
#. Bring text to lower case

** Section: selected - sign
   :PROPERTIES:
   :CUSTOM_ID: 91d54c
   :END:

#. Sign each paragraph in a section
#. Collect all paragraph signatures preserving their order
#. Combine using the delimiter (default: whitespace)
#. Sign the combined paragraph signatures

** Selected section: signature - insert
   :PROPERTIES:
   :CUSTOM_ID: d077a5
   :END:

#. Find the section reference
#. Enter a comment with the "section-id" prefix above the section reference
